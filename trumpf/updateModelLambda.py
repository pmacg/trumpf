import config
import json
import model
import codecs
if config.LIVE:
	import boto3

def respond():
	return {
		'statusCode': 200,
		'body': 'Success',
		'headers': {
			'Content-Type': 'application/json',
		},
	}

# Returns the filename of the downloaded tweets.
def downloadTweetsFromS3():
	if config.LIVE:
		s3 = boto3.resource('s3')
		s3.meta.client.download_file(config.S3_BUCKET,
			                         config.TWEETS_FILENAME,
			                         config.TEMP_TWEETS_FILENAME)
		lFilename = config.TEMP_TWEETS_FILENAME
	else:
		lFilename = config.LOCAL_BUCKET + config.TWEETS_FILENAME
	return lFilename

def saveTweetsToS3(tweetsFile):
	if config.LIVE:
		s3 = boto3.resource('s3')
		s3.meta.client.upload_file(config.TEMP_TWEETS_FILENAME,
								   config.S3_BUCKET,
								   config.TWEETS_FILENAME)

# Download the tweets from S3, add the new tweet and upload the file to S3.
def handleInput(event, context):
	print "Loading model from S3..."
	lFilename = downloadTweetsFromS3()
	lTweet = event['Records'][0]['Sns']['Message']
	print "Adding tweet to file: %s" % lTweet
	with codecs.open(lFilename, 'a', 'utf-8') as tweetFile:
		tweetFile.write(lTweet)
		tweetFile.write(u'\n')
	print "Saving tweets to S3..."
	saveTweetsToS3(tweetFile)
	return respond()

if __name__ == '__main__':
    handleInput(json.loads('{  "Records":[    {      "EventSource":"aws:sns",      "EventVersion": "1.0",      "EventSubscriptionArn": "arn:aws:sns:us-east-1:123456789012:lambda_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",      "Sns":{        "Type": "Notification",        "MessageId":"95df01b4-ee98-5cb9-9903-4c221d41eb5e",	"TopicArn":"arn:aws:sns:us-east-1:123456789012:lambda_topic",        "Subject":"TestInvoke",	"Message":"this is a test tweet",        "Timestamp":"2015-04-02T07:36:57.451Z",	"SignatureVersion":"1",	"Signature":"r0Dc5YVHuAglGcmZ9Q7SpFb2PuRDFmJNprJlAEEk8CzSq9Btu8U7dxOu++uU",        "SigningCertUrl":"http://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",	"UnsubscribeUrl":"http://cloudcast.amazon.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:123456789012:example_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",	"MessageAttributes":{"key":{"Type":"String","Value":"value"}}      }    }  ]}'), "don't read this")
    handleInput(json.loads('{  "Records":[    {      "EventSource":"aws:sns",      "EventVersion": "1.0",      "EventSubscriptionArn": "arn:aws:sns:us-east-1:123456789012:lambda_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",      "Sns":{        "Type": "Notification",        "MessageId":"95df01b4-ee98-5cb9-9903-4c221d41eb5e",	"TopicArn":"arn:aws:sns:us-east-1:123456789012:lambda_topic",        "Subject":"TestInvoke",	"Message":"I have tremendous respect for women and the many roles they serve that are vital to the fabric of our society and our economy.",        "Timestamp":"2015-04-02T07:36:57.451Z",	"SignatureVersion":"1",	"Signature":"r0Dc5YVHuAglGcmZ9Q7SpFb2PuRDFmJNprJlAEEk8CzSq9Btu8U7dxOu++uU",        "SigningCertUrl":"http://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",	"UnsubscribeUrl":"http://cloudcast.amazon.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:123456789012:example_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",	"MessageAttributes":{"key":{"Type":"String","Value":"value"}}      }    }  ]}'), "don't read this")
    