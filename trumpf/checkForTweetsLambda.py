import sys
import twitterUtils
import config
import json
from datetime import datetime, timedelta
if config.LIVE:
    import boto3

def respond():
    return {
        'statusCode': 200,
        'body': 'Success',
        'headers': {
            'Content-Type': 'application/json',
        },
    }

#-------------------------------------------------------------------------------
# isNewTweet
#
# Returns 'True' if the given tweet was created in the last 5 minutes
#-------------------------------------------------------------------------------
def isNewTweet(xiTweet):
    maximumAge = timedelta(minutes=5)    
    tweetTime = xiTweet.created_at
    nowTime = datetime.utcnow()
    tweetAge = nowTime - tweetTime
    return tweetAge < maximumAge

#-------------------------------------------------------------------------------
# updateSNS
#
# Updates the 'trumpTweets' aws SNS stream with the new donald trump tweet.
#
# Input
#   xiTweet   a utf-8 encoded string
#-------------------------------------------------------------------------------
def updateSNS(xiTweet):
    # This does nothing if we're in testing mode.
    if config.TESTING:
        return
    
    lClient = boto3.client('sns')
    response = lClient.publish(TopicArn=config.SNS_ARN,
                              Message=xiTweet)

def handleInput(event, context):
    lApi = twitterUtils.getApi()
    print "Getting tweets..."
    lTweets = twitterUtils.getRecentTweets(lApi, config.DONALD_USERNAME)

    lNewTweets = [twitterUtils.unescape(t.text) for t in lTweets if isNewTweet(t)]

    for tweet in lNewTweets:
        print "Found tweet: %s" % (tweet)
        updateSNS(tweet)

    if len(lTweets) == 0:
        print "No tweets found."

    return respond()

if __name__ == '__main__':
    handleInput("shouln't matter", "don't read this")