"""This script is used to package the zip file for uploading to amazon lambda.
It will:
 - Fill in the config template with the live info
 - Delete all .pyc files in the project directory
 - Create the zip file for uploading to AWS
"""
import fnmatch
import os
import zipfile
import glob

# A list of config values to be replaced. These values must all be defined
# as environment variables.
CONFIG_VALUES = ["TESTING",
                 "CONSUMER_KEY",
                 "CONSUMER_SECRET",
                 "ACCESS_TOKEN",
                 "ACCESS_SECRET",
                 "SNS_ARN",
                 "AWS_ACCESS_KEY",
                 "AWS_SECRET_KEY",]

def fillConfigTemplate():
    """Fills in the config.py.template file using environment variables"""
    templateFilename = "config.py.template"
    configFilename = "config.py"
    replaceString = "REPLACE_ME"

    print "Filling in config template..."

    if os.path.isfile(configFilename):
        print "  Removing existing config file"
        os.remove(configFilename)

    with open(templateFilename, 'r') as templateFile:
        with open(configFilename, 'w') as configFile:
            for line in templateFile:
                foundValue = False
                for value in CONFIG_VALUES:
                    if value in line and replaceString in line:
                        print "  Setting value: %s" % (value)
                        foundValue = True
                        newLine = "%s = %s\n" % (value, os.environ[value])
                        configFile.write(newLine)

                if not foundValue:
                    # This is an ordinary line without any configured value.
                    if replaceString in line:
                        raise "Unknown config option"
                    configFile.write(line)

    pass

def deletePycFiles():
    """Delete all .pyc files in the repository"""
    print "Deleting .pyc files..."

    # Create a list of all .pyc files in the directory.
    pycFiles = []
    for root, _, filenames in os.walk('.'):
        for filename in fnmatch.filter(filenames, '*.pyc'):
            pycFiles.append(os.path.join(root, filename))

    # Delete the .pyc files.
    for f in pycFiles:
        print "  Deleting %s" % (f)
        os.remove(f)

def createZip():
    """Create trumpf.zip for uploading to amazon lambda"""
    TRUMPF_FILE = "../trumpf.zip"

    print "Creating trumpf.zip..."

    if os.path.isfile(TRUMPF_FILE):
        print "  Removing existing trumpf.zip"
        os.remove(TRUMPF_FILE)

    with zipfile.ZipFile(TRUMPF_FILE, 'w') as newZip:
        print "  Create new trumpf.zip file."
        for root, _, filenames in os.walk('.'):
            for filename in filenames:
                print "Adding file", filename
                newZip.write(os.path.join(root, filename))

def package():
    """Do the work of packaging up the code"""
    fillConfigTemplate()
    deletePycFiles()
    createZip()

if __name__ == "__main__":
    package()
