import sys
import config
import tweepy

def unescape(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    # this has to be last:
    s = s.replace("&amp;", "&")
    return s

#-------------------------------------------------------------------------------
# getRecentTweets
# 
# Return a list of the most recent tweets from a given user.
# 
# Inputs
#   xiApi      the twitter api object to use. This shoulld already be
#              authenticated
#   xiUsername the username of the user whose tweets you would like to view.
#
# Returns
#   xoTweetList a list of tweet objects. The most recent tweet will be first in
#               the list.
#-------------------------------------------------------------------------------
def getRecentTweets(xiApi, xiUsername):
    xoTweetList = xiApi.user_timeline(screen_name=xiUsername,
                                      count=200)
    return xoTweetList

#-------------------------------------------------------------------------------
# getTweetsSinceId
# 
# Return a list of tweets from a given user since the given id.
# 
# Inputs
#   xiApi      the twitter api object to use. This shoulld already be
#              authenticated
#   xiUsername the username of the user whose tweets you would like to view.
#   xiId       Integer. only return tweets with a greater ID (more recent) than
#              this
#
# Returns
#   xoTweetList a list of utf-8 strings. The most recent tweet will be first in
#               the list.
#   xoLatestId  the ID of the latest tweet
#-------------------------------------------------------------------------------
def getTweetsSinceId(xiApi, xiUsername, xiId):
    #initialize a list to hold all the tweepy Tweets
    alltweets = []

    #make initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = xiApi.user_timeline(screen_name=xiUsername,
                                     count=200,
                                     since_id=xiId)

    #keep grabbing tweets until there are no tweets left to grab
    while len(new_tweets) > 0:
        # save the most recent tweets
        alltweets.extend(new_tweets)

        # save the id of the oldest tweet less one
        oldest = alltweets[-1].id - 1

        #all subsiquent requests use the max_id param to prevent duplicates
        new_tweets = xiApi.user_timeline(screen_name=xiUsername,
                                         count=200,
                                         max_id=oldest,
                                         since_id=xiId)

    #transform the tweepy tweets into a 2D array that will populate the csv
    outtweets = [unescape(tweet.text) for tweet in alltweets]
    latestId = alltweets[0].id_str if alltweets else xiId

    return (outtweets, latestId)

#-------------------------------------------------------------------------------
# downloadAllTweets
# 
# Return a list of all tweets from a given user.
# 
# Inputs
#   xiApi      the twitter api object to use. This shoulld already be
#              authenticated
#   xiUsername the username of the user whose tweets you would like to view.
#
# Returns
#   xoTweetList a list of utf-8 strings. The most recent tweet will be first in
#               the list.
#-------------------------------------------------------------------------------
def downloadAllTweets(xiApi, xiUsername):
    return getTweetsSinceId(xiApi, xiUsername, 1)[0]

#-------------------------------------------------------------------------------
# getApi()
#
# Uses the credentials in config to create and authenticate a tweepy API.
#
# Returns
#   xoApi   the api
#-------------------------------------------------------------------------------
def getApi():
    auth = tweepy.OAuthHandler(config.CONSUMER_KEY, config.CONSUMER_SECRET)
    auth.set_access_token(config.ACCESS_TOKEN, config.ACCESS_SECRET)
    xoApi = tweepy.API(auth)
    return xoApi