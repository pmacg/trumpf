import sys
import codecs
import tweepy
import twitterUtils

consumer_key = "GBcnLU9U6lrUM99gJH2qiVXCZ"
consumer_secret = "FpfH2bttPrr5EuOlsXPGtdaWJTg1ZzWfeMSDCS0DEtl7WKp56x"
access_token = "587697472-slNXhxV0uCgopvc7f6nbOpEtw1klaI04GrkX656O"
access_token_secret = "y5R5hmKKIJmi2H1RBrzydqk8dBUAylY5qp5UqDZj5h2wn"

def get_all_tweets(screen_name):
    #Twitter only allows access to a users most recent 3240 tweets with this method
    
    #authorize twitter, initialize tweepy
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    
    #initialize a list to hold all the tweepy Tweets
    alltweets = []  
    
    #make initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = api.user_timeline(screen_name = screen_name,count=200)
    
    #save most recent tweets
    alltweets.extend(new_tweets)
    
    #save the id of the oldest tweet less one
    oldest = alltweets[-1].id - 1
    
    #keep grabbing tweets until there are no tweets left to grab
    while len(new_tweets) > 0:
        print("getting tweets before %s" % (oldest))
        
        #all subsiquent requests use the max_id param to prevent duplicates
        new_tweets = api.user_timeline(screen_name = screen_name,count=200,max_id=oldest)
        
        #save most recent tweets
        alltweets.extend(new_tweets)
        
        #update the id of the oldest tweet less one
        oldest = alltweets[-1].id - 1
        
        print("...%s tweets downloaded so far" % (len(alltweets)))
    
    #transform the tweepy tweets into a 2D array that will populate the csv 
    outtweets = [[tweet.id_str, tweet.created_at, twitterUtils.unescape(tweet.text)] for tweet in alltweets]
    
    #write the csv  
    with codecs.open('%s_tweets.txt' % screen_name, 'w', 'utf-8') as f:
        for tweet in outtweets:
            f.write(tweet[2] + '\n')
            f.write('\n')

def removeBlankLines(filename):
    with open(filename, 'r') as fin:
        with open(filename + ".processed", 'w') as fout:
            for r in fin.readlines():
                if r.strip():
                    fout.write(r)

if __name__ == '__main__':
    get_all_tweets("realDonaldTrump")