"""
This is a script for uploading the trumpf.zip file to aws lambda. It is intended
for use by the CI pipeline.
"""

import os
import boto3

import config

ZIP_FILENAME = "../trumpf.zip"
FUNCTIONS = ["CheckForTweets",
             "UpdateTrumpModel",
             "TweetBack"]

def upload():
    lambda_client = boto3.client('lambda',
                                aws_access_key_id=config.AWS_ACCESS_KEY,
                                aws_secret_access_key=config.AWS_SECRET_KEY)

    if os.path.isfile(ZIP_FILENAME):
        with open(ZIP_FILENAME, 'rb') as fin:
            print "Reading", ZIP_FILENAME
            zip_bytes = fin.read()

        for func in FUNCTIONS:
            print "Updating", func
            lambda_client.update_function_code(FunctionName=func,
                                               ZipFile=zip_bytes)
    else:
        raise "Zip file does not exist. Try running package.py first."

if __name__ == "__main__":
    upload()