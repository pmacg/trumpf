import model
import config
import json
import twitterUtils
import markovify
import codecs
if config.LIVE:
    import boto3

def respond():
    return {
        'statusCode': 200,
        'body': 'Success',
        'headers': {
            'Content-Type': 'application/json',
        },
    }

def loadTweetsFromS3():
    if config.LIVE:
        s3 = boto3.resource('s3')
        s3.meta.client.download_file(config.S3_BUCKET,
                                     config.TWEETS_FILENAME,
                                     config.TEMP_TWEETS_FILENAME)
        lFilename = config.TEMP_TWEETS_FILENAME
    else:
        lFilename = config.LOCAL_BUCKET + config.TWEETS_FILENAME
    return lFilename

def generateNewTweet(xiTweetsFilename, xiInputTweet):
    with codecs.open(xiTweetsFilename, 'r', 'utf-8') as fin:
        text = fin.read()

    text_model = markovify.NewlineText(text)

    starting_words = tuple(text_model.word_split(xiInputTweet)[:2])

    try:
        new_tweet = text_model.make_short_sentence(140, 
                                                   init_state=starting_words)
    except:
        print "Couldn't create a tweet with the given starting words."
        print "Generating random tweet."
        new_tweet = text_model.make_short_sentence(140)

    return new_tweet

def tweet(xiTweet):
    lApi = twitterUtils.getApi()
    lApi.update_status(xiTweet)

def handleInput(event, context):
    print "Loading tweets from S3..."
    tweetsFile = loadTweetsFromS3()
    lInputTweet = event['Records'][0]['Sns']['Message']
    #lInputTweetID = int(event['Records'][0]['Sns']['Subject'])

    print "Generating new tweet from: %s" % (lInputTweet)
    lNewTweet = generateNewTweet(tweetsFile, lInputTweet)
    print "Generated tweet: %s" % (lNewTweet)

    print "Posting tweet"
    if config.LIVE and lNewTweet:
        tweet(lNewTweet)

    return respond()

if __name__ == '__main__':
    handleInput(json.loads('{  "Records":[    {      "EventSource":"aws:sns",      "EventVersion": "1.0",      "EventSubscriptionArn": "arn:aws:sns:us-east-1:123456789012:lambda_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",      "Sns":{        "Type": "Notification",        "MessageId":"95df01b4-ee98-5cb9-9903-4c221d41eb5e",	"TopicArn":"arn:aws:sns:us-east-1:123456789012:lambda_topic",        "Subject":"string",	"Message":"this is a test tweet",        "Timestamp":"2015-04-02T07:36:57.451Z",	"SignatureVersion":"1",	"Signature":"r0Dc5YVHuAglGcmZ9Q7SpFb2PuRDFmJNprJlAEEk8CzSq9Btu8U7dxOu++uU",        "SigningCertUrl":"http://sns.us-east-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",	"UnsubscribeUrl":"http://cloudcast.amazon.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:123456789012:example_topic:0b6941c3-f04d-4d3e-a66d-b1df00e1e381",	"MessageAttributes":{"key":{"Type":"String","Value":"value"}}      }    }  ]}'), "don't read this")