import sys
import markovify
import twitterUtils
import config
import codecs


def create_tweets_file():
    api = twitterUtils.getApi()
    tweets = twitterUtils.downloadAllTweets(api, config.DONALD_USERNAME)
    with codecs.open(config.LOCAL_BUCKET + config.TWEETS_FILENAME, 'w', 'utf-8') as fout:
        for tweet in tweets:
            fout.write(tweet)
            fout.write(u'\n')

with codecs.open(config.LOCAL_BUCKET + config.TWEETS_FILENAME, 'r', 'utf-8') as fin:
    text = fin.read()

text_model = markovify.NewlineText(text)

for i in range(5):
    print(text_model.make_short_sentence(140))
